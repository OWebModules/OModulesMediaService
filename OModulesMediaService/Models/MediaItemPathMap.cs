﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesMediaService.Models
{
    public class MediaItemPathMap
    {
        public string PathSrc { get; set; }
        public string PathDst { get; set; }
        public bool DeleteExistingDst { get; set; }
        public string IdFile { get; set; }
        public string NameFile { get; set; }
        public clsOntologyItem Result { get; set; }
    }
}
