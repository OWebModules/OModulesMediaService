﻿using MediaStore_Module;
using MediaStore_Module.Models;
using Nancy.Hosting.Self;
using Nancy.ModelBinding;
using OModulesMediaService.Primitives;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesMediaService
{
    
    class Program
    {
        private static Globals globals;
        private static MediaStoreConnector mediaStoreConnector;
        private static MediaServiceHost serviceHost;
        private static CommandLineOutput commandLineLogging = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);

        static void Main(string[] args)
        {
            globals = new Globals();

            if (mediaStoreConnector == null)
            {
                try
                {
                    mediaStoreConnector = new MediaStoreConnector(globals);
                    var controllerResultTask = mediaStoreConnector.GetMediaServiceHost();
                    controllerResultTask.Wait();

                    if (controllerResultTask.Result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        commandLineLogging.OutputError(controllerResultTask.Result.ResultState.Additional1);
                        Environment.Exit(-1);
                    }
                    serviceHost = controllerResultTask.Result.Result;
                }
                catch (Exception ex)
                {
                    commandLineLogging.OutputError(ex.Message);
                    Environment.Exit(-1);
                }
            }
            using (var host = new NancyHost(new Uri($"{serviceHost.Protocol.Name}://{serviceHost.Server.Name}:{ serviceHost.Port.Name }")))
            {
                host.Start();
                commandLineLogging.OutputInfo($"Running on { serviceHost.Protocol.Name}://{serviceHost.Server.Name}:{ serviceHost.Port.Name }");
                Console.ReadLine();

            }
        }
    }

    public class SampleModule: Nancy.NancyModule
    {
        public SampleModule()
        {
            Get["/"] = _ => "Hello World!";
        }
    }

    public class MediaStoreCopyModule : Nancy.NancyModule
    {
        private static Globals globals;
        private static MediaStoreConnector mediaStoreConnector;
        private static CommandLineOutput commandLineLogging = new CommandLineOutput(Properties.Settings.Default.LogOutput, Properties.Settings.Default.LogPath);

        private MediaItemPathMap PathMap
        {
            get; set;
        }

        private MediaItemPathMapList PathMapList { get; set; }
        public  MediaStoreCopyModule()
        {

            if (globals == null)
            {
                globals = new Globals();
            }

            if (mediaStoreConnector == null)
            {
                mediaStoreConnector = new MediaStore_Module.MediaStoreConnector(globals);
            }

            Get["/checkMediaServer"] = parameters =>
            {
                return CheckMediaServer();
            };

            Post["/createMediaItem"] = parameters =>
            {
                return CreateMediaItem();
            };

            Post["/copyFormSecondaryStore"] = parameters =>
            {
                return CopyFormSecondaryStore();
            };

            Post["/copyToMediaStore"] = parameters =>
            {
                return CopyToMediaStore();
            };

            Post["/deleteMedia"] = parameters =>
            {
                return DeleteMedia();
            };

            Post["/getCreateStamp"] = parameters =>
            {
                return GetCreateStamp();
            };
        }

        public MediaItemPathMap CreateMediaItem()
        {
            var model = this.Bind<MediaItemPathMap>();
            commandLineLogging.OutputInfo("Create MediaItem...");
            if (model == null)
            {
                model = new MediaItemPathMap
                {
                    Result = globals.LState_Error.Clone()
                };
                model.Result.Additional1 = Messages.ErrorModelIncorrect;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            if (string.IsNullOrEmpty(model.IdFile) ||
                !globals.is_GUID(model.IdFile))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorIdFile;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            };

            if (!File.Exists(model.PathSrc))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorFilePath;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            var dbReader = new OntologyModDBConnector(globals);
            var resultItem = dbReader.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = model.IdFile
                    }
                });

            if (resultItem.GUID == globals.LState_Error.GUID)
            {
                model.Result = resultItem;
                model.Result.Additional1 = Messages.ErrorFileItem;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            if (!dbReader.Objects1.Any())
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorFileItem;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            var oItemFile = dbReader.Objects1.First();
            oItemFile.Additional1 = model.PathSrc;

            if (oItemFile.GUID_Parent != mediaStoreConnector.ClassFile.GUID)
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorFileItemType;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            commandLineLogging.OutputInfo($"Write File: {oItemFile.Additional1}");
            model.Result = mediaStoreConnector.SaveFileToManagedMedia(oItemFile, oItemFile.Additional1);
            if (model.Result.GUID == globals.LState_Error.GUID)
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorSaveManagedFile;
                commandLineLogging.OutputError(model.Result.Additional1);
            }
            else
            {
                model.Result = globals.LState_Success.Clone();
                model.Result.Additional1 = Messages.SuccessManagedFile;
                commandLineLogging.OutputInfo($"Success: {model.Result.Additional1}");
            }

            PathMap = model;
            return model;
        }

        public bool CheckMediaServer()
        {
            commandLineLogging.OutputInfo("Check MediaServer: OK");
            var result = true;

            return result;
        }

        public MediaItemPathMap CopyFormSecondaryStore()
        {
            commandLineLogging.OutputInfo($"Copy from Secondary Store...");
            var model = this.Bind<MediaItemPathMap>();

            if (model == null)
            {
                model = new MediaItemPathMap
                {
                    Result = globals.LState_Error.Clone()
                };
                model.Result.Additional1 = Messages.ErrorModelIncorrect;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }


            if (string.IsNullOrEmpty(model.PathDst))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorDestPath;
                commandLineLogging.OutputError(model.Result.Additional1);
            }

            var pathSrc = model.PathSrc;

            if (!File.Exists(pathSrc))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = "Src does not exist!";
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            var pathDst = model.PathDst;

            var folder = Directory.GetDirectoryRoot(pathDst);
            if (!Directory.Exists(folder))
            {
                try
                {
                    Directory.CreateDirectory(folder);
                }
                catch (Exception ex)
                {
                    model.Result = globals.LState_Error.Clone();
                    model.Result.Additional1 = ex.Message;
                    commandLineLogging.OutputError(model.Result.Additional1);
                    return model;

                }
            }

            if (model.DeleteExistingDst)
            {
                try
                {
                    System.IO.File.Delete(pathDst);
                }
                catch (Exception ex)
                {
                    model.Result = globals.LState_Error.Clone();
                    model.Result.Additional1 = ex.Message;
                    commandLineLogging.OutputError(model.Result.Additional1);
                    return model;

                }
            }
            try
            {
                commandLineLogging.OutputInfo($"Copy from {pathSrc} to {pathDst}...");
                System.IO.File.Copy(pathSrc, pathDst);
                commandLineLogging.OutputInfo($"Copy from {pathSrc} to {pathDst}: OK");
            }
            catch (Exception ex)
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = ex.Message;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;

            }

            model.Result = globals.LState_Success.Clone();
            PathMap = model;
            return model;
        }

        public MediaItemPathMap CopyToMediaStore()
        {
            commandLineLogging.OutputInfo($"Copy to Media Store...");
            var model = this.Bind<MediaItemPathMap>();

            if (model == null)
            {
                model = new MediaItemPathMap();
                model.Result.Additional1 = Messages.ErrorModelIncorrect;
                return model;
            }

            model.Result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(model.IdFile) ||
                !globals.is_GUID(model.IdFile))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorIdFile;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            };

            if (string.IsNullOrEmpty(model.PathDst))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorDestPath;
                commandLineLogging.OutputError(model.Result.Additional1);
            }

            var dbReader = new OntologyModDBConnector(globals);
            var resultItem = dbReader.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = model.IdFile
                    }
                });

            if (resultItem.GUID == globals.LState_Error.GUID)
            {
                model.Result = resultItem;
                model.Result.Additional1 = Messages.ErrorFileItem;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            if (!dbReader.Objects1.Any())
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorFileItem;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            var path = model.PathDst;

            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    model.Result = globals.LState_Error.Clone();
                    model.Result.Additional1 = ex.Message;
                    commandLineLogging.OutputError(model.Result.Additional1);
                    return model;

                }
            }
            var oItemFile = dbReader.Objects1.First();

            var extension = Path.GetExtension(oItemFile.Name);
            var pathFile = Path.Combine(path, model.IdFile + extension);


            if (File.Exists(pathFile))
            {
                if (model.DeleteExistingDst)
                {
                    try
                    {
                        File.Delete(pathFile);
                    }
                    catch (Exception ex)
                    {
                        model.Result = globals.LState_Error.Clone();
                        model.Result.Additional2 = pathFile;
                        model.Result.Additional1 = ex.Message;
                        commandLineLogging.OutputError(model.Result.Additional1);
                        return model;
                    }
                }
                else
                {
                    model.Result = globals.LState_Nothing.Clone();
                    model.Result.Additional2 = pathFile;
                    commandLineLogging.OutputError(model.Result.Additional1);
                    return model;
                }

            }

            oItemFile.Additional1 = pathFile;

            if (oItemFile.GUID_Parent != mediaStoreConnector.ClassFile.GUID)
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorFileItemType;
                model.Result.Additional2 = pathFile;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }
            commandLineLogging.OutputInfo($"Write File: {oItemFile.Additional1}");
            model.Result = mediaStoreConnector.SaveManagedMediaToFile(oItemFile, oItemFile.Additional1, useFileSync: true);
            if (model.Result.GUID == globals.LState_Error.GUID)
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorGetManagedFile;
                model.Result.Additional2 = pathFile;
                commandLineLogging.OutputError(model.Result.Additional1);
            }
            else
            {
                model.Result = globals.LState_Success.Clone();
                model.Result.Additional2 = pathFile;
                model.Result.Additional1 = string.Format(Messages.SuccessSavedFile, model.Result.Additional2);
                commandLineLogging.OutputInfo($"Success: {model.Result.Additional1}");
            }

            PathMap = model;
            return model;
        }

        public MediaItemPathMap DeleteMedia()
        {
            commandLineLogging.OutputInfo($"Delete Media...");
            var model = this.Bind<MediaItemPathMap>();

            if (model == null)
            {
                model = new MediaItemPathMap
                {
                    Result = globals.LState_Error.Clone()
                };
                model.Result.Additional1 = Messages.ErrorModelIncorrect;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            if (string.IsNullOrEmpty(model.IdFile))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorIdFile;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            };

            var dbReader = new OntologyModDBConnector(globals);
            var resultItem = dbReader.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = model.IdFile
                    }
                });

            if (resultItem.GUID == globals.LState_Error.GUID)
            {
                model.Result = resultItem;
                model.Result.Additional1 = Messages.ErrorFileItem;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            if (!dbReader.Objects1.Any())
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorFileItem;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            var oItemFile = dbReader.Objects1.First();

            commandLineLogging.OutputInfo($"Delete Media: {oItemFile.Name}");
            model.Result = mediaStoreConnector.DelMedia(oItemFile);
            if (model.Result.GUID == globals.LState_Error.GUID)
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorDelManagedFile;
                commandLineLogging.OutputError(model.Result.Additional1);
            }
            else
            {
                model.Result = globals.LState_Success.Clone();
                model.Result.Additional1 = Messages.SuccessDelManagedFile;
                commandLineLogging.OutputInfo($"Success: {model.Result.Additional1}");
            }

            PathMap = model;
            return model;
        }

        public MediaItemPathMap GetCreateStamp()
        {
            commandLineLogging.OutputInfo($"Get Create Stamp...");
            var model = this.Bind<MediaItemPathMap>();
            if (model == null)
            {
                model = new MediaItemPathMap
                {
                    Result = globals.LState_Error.Clone()
                };
                model.Result.Additional1 = Messages.ErrorModelIncorrect;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }

            if (string.IsNullOrEmpty(model.IdFile))
            {
                model.Result = globals.LState_Error.Clone();
                model.Result.Additional1 = Messages.ErrorIdFile;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            };


            model.PathDst = mediaStoreConnector.GetMediaBlobPath(new clsOntologyItem { GUID = model.IdFile });
            if (!string.IsNullOrEmpty(model.PathDst))
            {

                var fileInfo = new FileInfo(model.PathDst);
                model.CreateStamp = fileInfo.CreationTime;
                commandLineLogging.OutputInfo($"Have Stamp: {model.CreateStamp.ToString()}");
            }

            commandLineLogging.OutputInfo($"Have Create Stamp.");

            PathMap = model;
            return model;
        }

        public MediaItemPathMapList GetCreateStampByList()
        {
            commandLineLogging.OutputInfo($"Get Create Stamp...");
            var model = this.Bind<MediaItemPathMapList>();
            var mediaItemPathMaps = this.Bind<List<MediaItemPathMap>>();
            model.MediaItemPathMaps = mediaItemPathMaps.ToArray();

            if (model == null)
            {
                model = new MediaItemPathMapList
                {
                    Result = globals.LState_Error.Clone()
                };
                model.Result.Additional1 = Messages.ErrorModelIncorrect;
                commandLineLogging.OutputError(model.Result.Additional1);
                return model;
            }
            
            foreach (var mapItem in model.MediaItemPathMaps)
            {
                if (string.IsNullOrEmpty(mapItem.IdFile))
                {
                    model.Result = globals.LState_Error.Clone();
                    model.Result.Additional1 = Messages.ErrorIdFile;
                    commandLineLogging.OutputError(model.Result.Additional1);
                    return model;
                };


                mapItem.PathDst = mediaStoreConnector.GetMediaBlobPath(new clsOntologyItem { GUID = mapItem.IdFile });
                if (!string.IsNullOrEmpty(mapItem.PathDst))
                {

                    var fileInfo = new FileInfo(mapItem.PathDst);
                    mapItem.CreateStamp = fileInfo.CreationTime;
                }
            }

            PathMapList = model;
            return model;
        }
    }

}
