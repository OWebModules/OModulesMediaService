$path = "$Env:ChocolateyInstall\lib\OModulesMediaService\OModulesMediaService\OModulesMediaService.exe.config"
[xml]$xmlDoc = Get-Content $path
$elements = $xmlDoc.GetElementsByTagName("OModulesMediaService.Properties.Settings")

$port = "9001"
$user = "Tassilo"

ForEach ($setting in $elements[0].ChildNodes)
{
    
    $nameAttrib = $setting.Attributes["name"]
    $value = $setting.ChildNodes
    
    if ($nameAttrib.Value -eq "Port") 
    {
        
        $port = $value[0].innerText
    }
    if ($nameAttrib.Value -eq "User")
    {
        $user = $value[0].innerText
    }
}

$url = "http://+:$port/"
Write-Host "netsh http add urlacl url=$url user=$user"
& netsh http add urlacl url=$url user=$user
